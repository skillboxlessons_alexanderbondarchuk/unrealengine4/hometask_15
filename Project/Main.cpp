#include <iostream>

void fun(bool isEven, int n) {
	for (int i = 0; i <= n; i++)
	{
		bool zeroRemainder{ i % 2 == 0 };

		if (isEven == zeroRemainder) {
			std::cout << i << " ";
		}
	}
}

void fun2(bool isEven, int n) {
	auto i = isEven ? 0 : 1;

	for (i; i <= n; i += 2)
	{
		std::cout << i << " ";
	}
}

void main() {
	const int N{ 5 };
	
	std::cout << "Even digits (fun): ";
	fun(true, N);

	std::cout << std::endl << "Odd digits (fun): ";
	fun(false, N);

	std::cout << std::endl << "Even digits (fun2): ";
	fun2(true, N);

	std::cout << std::endl << "Odd digits (fun2): ";
	fun2(false, N);
}